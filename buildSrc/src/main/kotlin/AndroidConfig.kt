import AndroidConfig.DEFAULT_DIMENSION

object AndroidConfig {
    const val COMPILE_SDK_VERSION = 29
    const val MIN_SDK_VERSION = 17
    const val TARGET_SDK_VERSION = 28
    const val BUILD_TOOLS_VERSION = "29.0.0"

    const val VERSION_CODE = 1020
    const val VERSION_NAME = "2.1.5"

    const val ID = "com.kiloosoftware.foodino"
    const val TEST_INSTRUMENTATION_RUNNER = "android.support.test.runner.AndroidJUnitRunner"

    const val DEFAULT_DIMENSION = "build"
}

interface BuildType {

    companion object {
        const val RELEASE = "release"
        const val DEBUG = "debug"
    }

    val isMinifyEnabled: Boolean
    val isMultiDexEnabled: Boolean
}

interface ProductFlavor {

    companion object {
        const val DEV = "dev"
        const val STAGING = "staging"
        const val PRODUCTION = "production"
        const val MOCK = "mock"
        const val API_BASE_URL = "API_BASE_URL"
        const val BASE_URL = "BASE_URL"
        const val MOCK_ENABLED = "MOCK_ENABLED"
        const val CLIENT_ID = "CLIENT_ID"
        const val CLIENT_SECRET = "CLIENT_SECRET"
    }

    val applicationIdSuffix: String
    val apiBaseUrl: String
    val baseUrl: String
    val mockEnabled: Boolean
    val dimension: String
    val clientId: String
    val clientSecret: String
}

object BuildTypeDebug : BuildType {
    override val isMinifyEnabled = false
    override val isMultiDexEnabled = true
}

object BuildTypeRelease : BuildType {
    override val isMinifyEnabled = true
    override val isMultiDexEnabled = true
}

object ProductFlavorDev : ProductFlavor {
    override val applicationIdSuffix = ".dev"
    override val apiBaseUrl = "dev.something"
    override val baseUrl = "dev.something"
    override val mockEnabled = false
    override val dimension = DEFAULT_DIMENSION
    override val clientId = ""
    override val clientSecret = ""
}

object ProductFlavorStaging : ProductFlavor {
    override val applicationIdSuffix = ".staging"
    override val apiBaseUrl = "staging.something"
    override val baseUrl = "staging.something"
    override val mockEnabled = false
    override val dimension = DEFAULT_DIMENSION
    override val clientId = ""
    override val clientSecret = ""
}

object ProductFlavorProduction : ProductFlavor {
    override val applicationIdSuffix = ""
    override val apiBaseUrl = "something"
    override val baseUrl = "something"
    override val mockEnabled = false
    override val dimension = DEFAULT_DIMENSION
    override val clientId = ""
    override val clientSecret = ""
}

object ProductFlavorMock : ProductFlavor {
    override val applicationIdSuffix = ".dev"
    override val apiBaseUrl = "\"\""
    override val baseUrl = "\"\""
    override val mockEnabled = true
    override val dimension = DEFAULT_DIMENSION
    override val clientId = "\"\""
    override val clientSecret = "\"\""
}

object TestOptions {
    const val IS_RETURN_DEFAULT_VALUES = true
}

