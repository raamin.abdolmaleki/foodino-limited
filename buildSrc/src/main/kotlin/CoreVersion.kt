// Versions consts that are used across libraries and Gradle plugins
object CoreVersion {
    const val KOTLIN = "1.3.50"
    const val COROUTINES_ANDROID = "1.3.8"
    const val NAVIGATION = "2.2.0"
}
