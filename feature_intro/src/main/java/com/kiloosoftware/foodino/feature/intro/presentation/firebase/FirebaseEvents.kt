package com.kiloosoftware.foodino.feature.intro.presentation.firebase

import com.google.firebase.analytics.FirebaseAnalytics

class FirebaseEvents(private val firebaseAnalytics: FirebaseAnalytics) {

    fun updateClicked() {
        firebaseAnalytics.logEvent(EventConstants.UPDATE_CLICKED, null)
    }
}
