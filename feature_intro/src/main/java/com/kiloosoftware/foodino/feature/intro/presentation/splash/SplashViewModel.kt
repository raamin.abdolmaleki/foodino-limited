package com.kiloosoftware.foodino.feature.intro.presentation.splash

import androidx.lifecycle.viewModelScope
import com.kiloosoftware.foodino.feature.intro.BuildConfig
import com.kiloosoftware.foodino.feature.intro.data.model.ConfigDataModel
import com.kiloosoftware.foodino.feature.intro.domain.usecase.GetConfigUseCase
import com.kiloosoftware.foodino.feature.intro.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.library.base.data.Error
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseAction
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewModel
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewState
import com.kiloosoftware.foodino.library.base.utils.sharedpreference.ConfigPreferences
import kotlinx.coroutines.launch

internal class SplashViewModel(
    private val getConfigUseCase: GetConfigUseCase,
    private val firebaseEvents: FirebaseEvents,
    private val configPreferences: ConfigPreferences
) : BaseViewModel<SplashViewModel.ViewState, SplashViewModel.Action>(ViewState()) {

    override fun onLoadData() {
        getConfig()
    }

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.NoUpdate -> state.copy(
            noUpdate = true,
            isUpdateReady = false,
            updateMessage = null,
            updateUrl = null,
            isForceUpdate = null,
            isLoading = false,
            isError = false,
            data = viewAction.data
        )
        is Action.UpdateReady -> state.copy(
            noUpdate = false,
            isUpdateReady = true,
            updateMessage = viewAction.updateMessage,
            updateUrl = viewAction.updateUrl,
            isForceUpdate = viewAction.isForce,
            isLoading = false,
            isError = false,
            data = viewAction.data
        )
        is Action.ConfigLoading -> state.copy(
            noUpdate = false,
            isUpdateReady = false,
            updateMessage = null,
            updateUrl = null,
            isForceUpdate = null,
            isLoading = true,
            isError = false,
            data = null
        )
        is Action.ConfigLoadingFailure -> state.copy(
            noUpdate = false,
            isUpdateReady = false,
            updateMessage = null,
            updateUrl = null,
            isForceUpdate = null,
            isLoading = false,
            isError = true,
            errors = viewAction.errors,
            data = null
        )
    }

    private fun getConfig() {
        sendAction(Action.ConfigLoading)
        viewModelScope.launch {
            getConfigUseCase.execute().also {
                if (it.statusCode == StatusCode.OK) {
                    val updateAction = getUpdateAction(it.data!!)
                    if (updateAction == null) {
                        sendAction(Action.NoUpdate(it.data!!))
                    } else {
                        sendAction(updateAction)
                    }
                    configPreferences.setConfig(
                        baseUrl = it.data!!.baseUrl,
                        restaurantImageUrl = it.data!!.restaurantImage ?: "",
                        mealImageUrl = it.data!!.mealImage ?: ""
                    )
                } else {
                    sendAction(Action.ConfigLoadingFailure(it.errors))
                }
            }
        }
    }

    private fun getUpdateAction(configDataModel: ConfigDataModel): Action? {
        val update = configDataModel.update
        if (update.forceUpdateEnabled && (update.forceUpdateMinVersion >=
                    BuildConfig.VERSION_CODE ||
                    BuildConfig.VERSION_CODE in update.forceUpdateIncludeVersions)
        ) {
            return Action.UpdateReady(
                updateMessage = update.forceUpdateMessage,
                updateUrl = update.forceUpdateUrl,
                isForce = true,
                data = configDataModel
            )
        } else {
            if (update.updateEnabled && (update.updateMinVersion >=
                        BuildConfig.VERSION_CODE ||
                        BuildConfig.VERSION_CODE in update.updateIncludeVersions)
            ) {
                return Action.UpdateReady(
                    updateMessage = update.updateMessage,
                    updateUrl = update.updateUrl,
                    isForce = false,
                    data = configDataModel
                )
            }
            return null
        }
    }

    fun updateClicked() = firebaseEvents.updateClicked()

    internal data class ViewState(
        val noUpdate: Boolean = false,
        val isUpdateReady: Boolean = false,
        val updateMessage: String? = null,
        val updateUrl: String? = null,
        val isForceUpdate: Boolean? = null,
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val errors: List<Error>? = null,
        val data: ConfigDataModel? = null
    ) : BaseViewState

    internal sealed class Action : BaseAction {
        class NoUpdate(val data: ConfigDataModel) : Action()
        class UpdateReady(
            val updateMessage: String,
            val updateUrl: String,
            val isForce: Boolean,
            val data: ConfigDataModel
        ) : Action()

        class ConfigLoadingFailure(val errors: List<Error>?) : Action()
        object ConfigLoading : Action()
    }
}
