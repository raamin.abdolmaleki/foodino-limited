package com.kiloosoftware.foodino.feature.intro.data.repository.mock

import com.kiloosoftware.foodino.feature.intro.data.model.ConfigDataModel
import com.kiloosoftware.foodino.feature.intro.data.model.UpdateDataModel
import com.kiloosoftware.foodino.feature.intro.domain.repository.ConfigRepository
import com.kiloosoftware.foodino.library.base.data.Resource
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.utils.Constants
import kotlinx.coroutines.delay

internal class ConfigRepositoryMockImpl : ConfigRepository {

    override suspend fun getAppConfig(): Resource<ConfigDataModel> {
        delay(Constants.MOCK_DELAY_TIME)

        val response = ConfigDataModel(
            update = UpdateDataModel(
                lastVersion = 990,
                updateEnabled = true,
                updateMinVersion = 980,
                updateIncludeVersions = listOf(988),
                updateMessage = "بیا آپدیت کن",
                updateUrl = "",
                forceUpdateEnabled = false,
                forceUpdateMinVersion = 970,
                forceUpdateIncludeVersions = listOf(978),
                forceUpdateMessage = "حتما باید بیای آپدیت کنی",
                forceUpdateUrl = ""
            ),
            baseUrl = "foodino.ir",
            restaurantImage = "https://svgshare.com/i/NVP.svg",
            mealImage = "https://svgshare.com/i/NVj.svg"
        )
        return Resource(
            data = response,
            statusCode = StatusCode.OK
        )
    }
}
