package com.kiloosoftware.foodino.feature.intro.data.model

import com.squareup.moshi.Json

internal data class ConfigDataModel(
    @field:Json(name = "update") val update: UpdateDataModel,
    @field:Json(name = "base_url") val baseUrl: String?,
    @field:Json(name = "restaurant_image") val restaurantImage: String?,
    @field:Json(name = "meal_image") val mealImage: String?,
)
