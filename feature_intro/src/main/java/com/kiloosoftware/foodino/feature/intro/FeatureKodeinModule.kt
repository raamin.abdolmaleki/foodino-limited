package com.kiloosoftware.foodino.feature.intro

import com.kiloosoftware.foodino.feature.KodeinModuleProvider
import com.kiloosoftware.foodino.feature.intro.data.dataModule
import com.kiloosoftware.foodino.feature.intro.domain.domainModule
import com.kiloosoftware.foodino.feature.intro.presentation.presentationModule

import org.kodein.di.Kodein

internal const val FEATURE_NAME = "Intro"

object FeatureKodeinModule : KodeinModuleProvider {

    override val kodeinModule = Kodein.Module("${FEATURE_NAME}Module") {
        import(presentationModule)
        import(domainModule)
        import(dataModule)
    }
}
