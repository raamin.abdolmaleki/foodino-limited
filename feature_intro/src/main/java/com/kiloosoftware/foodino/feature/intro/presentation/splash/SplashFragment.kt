package com.kiloosoftware.foodino.feature.intro.presentation.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.pawegio.kandroid.visible
import com.kiloosoftware.foodino.feature.intro.BuildConfig
import com.kiloosoftware.foodino.feature.intro.R
import com.kiloosoftware.foodino.feature.intro.databinding.FragmentSplashBinding
import com.kiloosoftware.foodino.library.base.data.Error
import com.kiloosoftware.foodino.library.base.presentation.extension.observe
import com.kiloosoftware.foodino.library.base.presentation.extension.setOnDebouncedClickListener
import com.kiloosoftware.foodino.library.base.presentation.fragment.BaseContainerFragment
import com.kiloosoftware.foodino.presentation.singleton.AppBarState
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarData
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarStyle
import kotlinx.android.synthetic.main.fragment_splash.*
import org.kodein.di.generic.instance

class SplashFragment : BaseContainerFragment<FragmentSplashBinding>() {

    private val viewModel: SplashViewModel by instance()

    override val layoutResourceId = R.layout.fragment_splash

    private val stateObserver = Observer<SplashViewModel.ViewState> {
        binding.loading = it.isLoading
        binding.errorMessage = it.errors?.first()?.message

        when {
            it.noUpdate -> {
                goToHomepage()
            }
            it.isUpdateReady -> {
                showUpdateDialog(it.updateMessage!!, it.updateUrl!!, it.isForceUpdate!!)
            }
            it.isError -> {
                showErrors(it.errors) {
                    viewModel.loadData()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.versionName = String.format(getString(R.string.version_name_placeholder), BuildConfig.VERSION_NAME)

        binding.loading = true

        observe(viewModel.stateLiveData, stateObserver)
        viewModel.loadData()
    }

    @SuppressLint("InflateParams")
    private fun showUpdateDialog(message: String, url: String, forceUpdate: Boolean) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_update, null, false)
        builder.setView(dialogView)
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)

        dialogView.findViewById<AppCompatTextView>(R.id.tvUpdateMessage).text = message

        val btnLater = dialogView.findViewById<Button>(R.id.btnLater)
        btnLater.visible = !forceUpdate
        btnLater.setOnClickListener {
            alertDialog.dismiss()
            goToHomepage()
        }
        dialogView.findViewById<Button>(R.id.btnUpdate).setOnClickListener {
            viewModel.updateClicked()
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context?.packageManager?.let {
                startActivity(intent)
            }
        }

        alertDialog.show()
    }

    override fun showErrors(errors: List<Error>?, retry: () -> Any) {
        binding.errorMessage = if (errors.isNullOrEmpty()) {
            resources.getString(R.string.error_message_unknown)
        } else {
            errors.first().message
        }

        btnRetry.setOnDebouncedClickListener {
            retry()
        }
    }

    private fun goToHomepage() {
        findNavController().navigate(SplashFragmentDirections.actionSplashToHomepage())
    }

    override fun setToolbar() {
        AppBarState.style = AppBarData(AppBarStyle.NO_APP_BAR)
    }
}
