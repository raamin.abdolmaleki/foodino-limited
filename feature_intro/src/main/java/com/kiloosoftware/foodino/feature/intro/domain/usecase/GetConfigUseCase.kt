package com.kiloosoftware.foodino.feature.intro.domain.usecase

import com.kiloosoftware.foodino.feature.intro.domain.repository.ConfigRepository

internal class GetConfigUseCase(
    private val configRepository: ConfigRepository
) {
    suspend fun execute() = configRepository.getAppConfig()
}
