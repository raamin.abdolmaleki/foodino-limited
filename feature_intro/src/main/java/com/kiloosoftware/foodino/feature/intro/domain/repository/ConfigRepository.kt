package com.kiloosoftware.foodino.feature.intro.domain.repository

import com.kiloosoftware.foodino.feature.intro.data.model.ConfigDataModel
import com.kiloosoftware.foodino.library.base.data.Resource

internal interface ConfigRepository {
    suspend fun getAppConfig(): Resource<ConfigDataModel>
}
