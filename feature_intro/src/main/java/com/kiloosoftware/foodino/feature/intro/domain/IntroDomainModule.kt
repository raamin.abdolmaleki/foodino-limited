package com.kiloosoftware.foodino.feature.intro.domain

import com.kiloosoftware.foodino.feature.intro.FEATURE_NAME
import com.kiloosoftware.foodino.feature.intro.domain.usecase.GetConfigUseCase
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val domainModule = Kodein.Module("${FEATURE_NAME}DomainModule") {
    bind() from singleton { GetConfigUseCase(instance()) }
}
