package com.kiloosoftware.foodino.feature.intro.data.retrofit.service

import com.kiloosoftware.foodino.feature.intro.data.model.ConfigDataModel
import com.kiloosoftware.foodino.library.base.data.ApiResponse
import retrofit2.Response
import retrofit2.http.GET

internal interface ConfigRetrofitService {
    @GET("android/configs")
    suspend fun getConfigAsync(): Response<ApiResponse<ConfigDataModel>>
}
