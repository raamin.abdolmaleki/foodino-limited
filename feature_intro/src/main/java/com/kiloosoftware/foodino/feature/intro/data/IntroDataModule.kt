package com.kiloosoftware.foodino.feature.intro.data

import com.kiloosoftware.foodino.feature.intro.BuildConfig
import com.kiloosoftware.foodino.feature.intro.FEATURE_NAME
import com.kiloosoftware.foodino.feature.intro.data.repository.ConfigRepositoryImpl
import com.kiloosoftware.foodino.feature.intro.data.repository.mock.ConfigRepositoryMockImpl
import com.kiloosoftware.foodino.feature.intro.data.retrofit.service.ConfigRetrofitService
import com.kiloosoftware.foodino.feature.intro.domain.repository.ConfigRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("${FEATURE_NAME}DataModule") {

    // if in mock flavor then mock data will be return
    bind<ConfigRepository>() with singleton {
        if (!BuildConfig.MOCK_ENABLED) ConfigRepositoryImpl(instance()) else ConfigRepositoryMockImpl()
    }

    bind() from singleton { instance<Retrofit>().create(ConfigRetrofitService::class.java) }
}
