package com.kiloosoftware.foodino.feature.intro.data.model

import com.squareup.moshi.Json

internal data class UpdateDataModel(
    @field:Json(name = "latest_version") val lastVersion: Int,
    @field:Json(name = "update_enabled") val updateEnabled: Boolean,
    @field:Json(name = "update_min_version") val updateMinVersion: Int,
    @field:Json(name = "update_include_versions") val updateIncludeVersions: List<Int>,
    @field:Json(name = "update_message") val updateMessage: String,
    @field:Json(name = "update_url") val updateUrl: String,
    @field:Json(name = "force_update_enabled") val forceUpdateEnabled: Boolean,
    @field:Json(name = "force_update_min_version") val forceUpdateMinVersion: Int,
    @field:Json(name = "force_update_include_versions") val forceUpdateIncludeVersions:List<Int>,
    @field:Json(name = "force_update_message") val forceUpdateMessage: String,
    @field:Json(name = "force_update_url") val forceUpdateUrl: String
)
