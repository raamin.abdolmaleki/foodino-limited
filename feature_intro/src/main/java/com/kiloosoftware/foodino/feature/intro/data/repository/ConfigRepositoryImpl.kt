package com.kiloosoftware.foodino.feature.intro.data.repository

import com.kiloosoftware.foodino.feature.intro.data.model.ConfigDataModel
import com.kiloosoftware.foodino.feature.intro.data.retrofit.service.ConfigRetrofitService
import com.kiloosoftware.foodino.feature.intro.domain.repository.ConfigRepository
import com.kiloosoftware.foodino.library.base.data.Resource
import com.kiloosoftware.foodino.library.base.data.safeApiCall

internal class ConfigRepositoryImpl(
    private val configRetrofitService: ConfigRetrofitService
) : ConfigRepository {

    override suspend fun getAppConfig(): Resource<ConfigDataModel> {
        return safeApiCall(call = { configRetrofitService.getConfigAsync() })
    }
}
