package com.kiloosoftware.foodino.feature.intro.presentation

import android.content.Context
import androidx.fragment.app.Fragment
import com.kiloosoftware.foodino.feature.intro.FEATURE_NAME
import com.kiloosoftware.foodino.feature.intro.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.feature.intro.presentation.splash.SplashViewModel
import com.kiloosoftware.foodino.library.base.di.KotlinViewModelProvider
import com.kiloosoftware.foodino.library.base.utils.sharedpreference.ConfigPreferences
import com.kiloosoftware.foodino.library.base.utils.sharedpreference.UserPreferences
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.*

internal val presentationModule = Kodein.Module("${FEATURE_NAME}PresentationModule") {

    bind<FirebaseEvents>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        FirebaseEvents(instance())
    }

    bind<ConfigPreferences>() with factory { context: Context ->
        ConfigPreferences(
            context
        )
    }

    bind<ConfigPreferences>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        ConfigPreferences(
            context.context!!
        )
    }

    bind<SplashViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) { SplashViewModel(instance(), instance(), instance()) }
    }
}
