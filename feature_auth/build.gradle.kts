import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

plugins {
    id(GradlePluginId.ANDROID_DYNAMIC_FEATURE)
    id(GradlePluginId.KOTLIN_ANDROID)
    id(GradlePluginId.KOTLIN_ANDROID_EXTENSIONS)
    id(GradlePluginId.SAFE_ARGS)
    id(GradlePluginId.KOTLIN_KAPT)
}

android {
    compileSdkVersion(AndroidConfig.COMPILE_SDK_VERSION)

    defaultConfig {
        minSdkVersion(AndroidConfig.MIN_SDK_VERSION)
        targetSdkVersion(AndroidConfig.TARGET_SDK_VERSION)

        vectorDrawables.useSupportLibrary = true

        versionCode = AndroidConfig.VERSION_CODE
        versionName = AndroidConfig.VERSION_NAME
        testInstrumentationRunner = AndroidConfig.TEST_INSTRUMENTATION_RUNNER
    }

    buildTypes {
        getByName(BuildType.RELEASE) {

        }

        getByName(BuildType.DEBUG) {
        }
    }

    flavorDimensions(AndroidConfig.DEFAULT_DIMENSION)

    productFlavors {

        create(ProductFlavor.DEV) {
            applicationIdSuffix = ProductFlavorDev.applicationIdSuffix
            buildConfigField("String", ProductFlavor.API_BASE_URL, ProductFlavorDev.apiBaseUrl)
            buildConfigField("String", ProductFlavor.BASE_URL, ProductFlavorDev.baseUrl)
            buildConfigField("String", ProductFlavor.CLIENT_ID, ProductFlavorDev.clientId)
            buildConfigField("String", ProductFlavor.CLIENT_SECRET, ProductFlavorDev.clientSecret)
            buildConfigField(
                "boolean",
                ProductFlavor.MOCK_ENABLED,
                ProductFlavorDev.mockEnabled.toString()
            )
            setDimension(ProductFlavorDev.dimension)
        }

        create(ProductFlavor.STAGING) {
            applicationIdSuffix = ProductFlavorStaging.applicationIdSuffix
            buildConfigField("String", ProductFlavor.API_BASE_URL, ProductFlavorStaging.apiBaseUrl)
            buildConfigField("String", ProductFlavor.BASE_URL, ProductFlavorStaging.baseUrl)
            buildConfigField("String", ProductFlavor.CLIENT_ID, ProductFlavorStaging.clientId)
            buildConfigField("String", ProductFlavor.CLIENT_SECRET, ProductFlavorStaging.clientSecret)
            buildConfigField(
                "boolean",
                ProductFlavor.MOCK_ENABLED,
                ProductFlavorStaging.mockEnabled.toString()
            )
            setDimension(ProductFlavorStaging.dimension)
        }

        create(ProductFlavor.PRODUCTION) {
            buildConfigField("String", ProductFlavor.API_BASE_URL, ProductFlavorProduction.apiBaseUrl)
            buildConfigField("String", ProductFlavor.BASE_URL, ProductFlavorProduction.baseUrl)
            buildConfigField("String", ProductFlavor.CLIENT_ID, ProductFlavorProduction.clientId)
            buildConfigField("String", ProductFlavor.CLIENT_SECRET, ProductFlavorProduction.clientSecret)
            buildConfigField(
                "boolean",
                ProductFlavor.MOCK_ENABLED,
                ProductFlavorProduction.mockEnabled.toString()
            )
            setDimension(ProductFlavorProduction.dimension)
        }

        create(ProductFlavor.MOCK) {
            applicationIdSuffix = ProductFlavorMock.applicationIdSuffix
            buildConfigField("String", ProductFlavor.API_BASE_URL, ProductFlavorProduction.apiBaseUrl)
            buildConfigField("String", ProductFlavor.BASE_URL, ProductFlavorProduction.baseUrl)
            buildConfigField("String", ProductFlavor.CLIENT_ID, ProductFlavorMock.clientId)
            buildConfigField("String", ProductFlavor.CLIENT_SECRET, ProductFlavorMock.clientSecret)
            buildConfigField(
                "boolean",
                ProductFlavor.MOCK_ENABLED,
                ProductFlavorMock.mockEnabled.toString()
            )
            setDimension(ProductFlavorProduction.dimension)
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    dataBinding.isEnabled = true

    kotlinOptions {
        // "this" is currently lacking a proper type
        // See: https://youtrack.jetbrains.com/issue/KT-31077
        val options = this as? KotlinJvmOptions
        options?.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    // This "test" source set is a fix for SafeArgs classes not being available when running Unit tests from cmd
    // See: https://issuetracker.google.com/issues/139242292
    sourceSets {
        getByName("test").java.srcDir("${project.rootDir}/app/build/generated/source/navigation-args/debug")
    }

    // Removes the need to mock need to mock classes that may be irrelevant from test perspective
    testOptions {
        unitTests.isReturnDefaultValues = TestOptions.IS_RETURN_DEFAULT_VALUES
    }
}

androidExtensions { isExperimental = true }

dependencies {
    implementation(project(ModuleDependency.APP))

    implementation(LibraryDependency.FIREBASE_ANALYTICS)
    implementation(LibraryDependency.ADJUST)

    addTestDependencies()
}
