package com.kiloosoftware.foodino.feature.auth.presentation.login

import androidx.lifecycle.viewModelScope
import com.kiloosoftware.foodino.feature.auth.data.model.response.AuthenticateUserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.VerificationCodeDataModel
import com.kiloosoftware.foodino.feature.auth.domain.usecase.LoginUseCase
import com.kiloosoftware.foodino.feature.auth.domain.usecase.SendVerificationCodeUseCase
import com.kiloosoftware.foodino.feature.auth.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.library.base.data.Error
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseAction
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewModel
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewState
import com.kiloosoftware.foodino.library.base.utils.sharedpreference.UserPreferences
import kotlinx.coroutines.launch

internal class LoginViewModel(
    private val loginUseCase: LoginUseCase,
    private val sendVerificationCodeUseCase: SendVerificationCodeUseCase,
    private val args: LoginFragmentArgs,
    private val userPreferences: UserPreferences,
    private val firebaseEvents: FirebaseEvents
) : BaseViewModel<LoginViewModel.ViewState, LoginViewModel.Action>(
    ViewState()
) {

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.LoginSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isLoginSuccess = true,
            isVerificationCodeSuccess= false,
            errors = null,
            loginData = viewAction.data,
            sendVerificationCodeData = null
        )
        is Action.RequestFailure -> state.copy(
            isLoading = false,
            isError = true,
            isLoginSuccess = false,
            isVerificationCodeSuccess = false,
            errors = viewAction.errors,
            loginData = null,
            sendVerificationCodeData = null
        )
        is Action.RequestLoading -> state.copy(
            isLoading = true,
            isError = false,
            isLoginSuccess = false,
            isVerificationCodeSuccess = false,
            errors = null,
            loginData = null,
            sendVerificationCodeData = null
        )
        is Action.SendVerificationCodeSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isLoginSuccess= false,
            isVerificationCodeSuccess = true,
            errors = null,
            loginData = null,
            sendVerificationCodeData = viewAction.data
        )
    }

    fun loginUser(password: String) {
        sendAction(Action.RequestLoading)
        viewModelScope.launch {
            loginUseCase.execute(args.phoneNumber, password, args.ott).also {
                if (it.statusCode == StatusCode.OK && it.data != null) {
                    userPreferences.setLoginInfo(
                        accessToken = it.data!!.accessToken,
                        expiresIn = it.data!!.expiresIn
                    )
                    firebaseEvents.login()
                    sendAction(Action.LoginSuccess(it.data!!))
                } else {
                    sendAction(Action.RequestFailure(it.errors))
                }
            }
        }
    }

    fun forgotPassword() {
        viewModelScope.launch {
            sendVerificationCodeUseCase.execute(args.phoneNumber).also {
                if (it.statusCode == StatusCode.OK && it.data != null) {
                    sendAction(Action.SendVerificationCodeSuccess(it.data!!))
                } else {
                    sendAction(Action.RequestFailure(it.errors))
                }
            }
        }
    }

    fun getArgs() = args

    internal data class ViewState(
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val isLoginSuccess: Boolean = false,
        val isVerificationCodeSuccess: Boolean = false,
        val errors: List<Error>? = null,
        val loginData: AuthenticateUserDataModel? = null,
        val sendVerificationCodeData: VerificationCodeDataModel? = null
    ) : BaseViewState

    internal sealed class Action : BaseAction {
        class LoginSuccess(val data: AuthenticateUserDataModel) : Action()
        class SendVerificationCodeSuccess(val data: VerificationCodeDataModel) : Action()
        class RequestFailure(val errors: List<Error>?) : Action()
        object RequestLoading : Action()
    }
}
