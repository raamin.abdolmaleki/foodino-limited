package com.kiloosoftware.foodino.feature.auth.presentation.register

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.kiloosoftware.foodino.library.base.presentation.extension.observe
import com.kiloosoftware.foodino.library.base.presentation.fragment.BaseContainerFragment
import com.kiloosoftware.foodino.feature.auth.R
import com.kiloosoftware.foodino.feature.auth.databinding.FragmentRegisterBinding
import com.kiloosoftware.foodino.library.base.presentation.extension.setOnDebouncedClickListener
import com.kiloosoftware.foodino.presentation.singleton.AppBarState
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarData
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarStyle
import kotlinx.android.synthetic.main.fragment_register.*
import org.kodein.di.generic.instance

class RegisterFragment : BaseContainerFragment<FragmentRegisterBinding>() {

    override val layoutResourceId = R.layout.fragment_register

    private val viewModel: RegisterViewModel by instance()

    private val stateObserver = Observer<RegisterViewModel.ViewState> {
        binding.loading = it.isLoading

        if (it.isSuccess) {
            // back to first page of auth process
            findNavController().popBackStack()
            // pop first page of auth and go back to starting process
            findNavController().popBackStack()
        } else if (it.isError) showErrors(it.errors) {
            registerUser()
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btnConfirmation.setOnDebouncedClickListener {
            registerUser()
        }

        observe(viewModel.stateLiveData, stateObserver)
    }

    private fun registerUser() {
        viewModel.registerUser(
            etFullName.editableText.toString(),
            etPassword.editableText.toString()
        )
    }

    override fun setToolbar() {
        AppBarState.style = AppBarData(AppBarStyle.TITLE_WITH_BACK)
    }
}
