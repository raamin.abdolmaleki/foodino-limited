package com.kiloosoftware.foodino.feature.auth.domain.usecase

import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository

internal class RegisterUseCase(
    private val authRepository: AuthRepository
) {
    suspend fun execute(fullName: String, password: String) =
        authRepository.registerUser(fullName, password)
}
