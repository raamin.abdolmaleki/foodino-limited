package com.kiloosoftware.foodino.feature.auth.data.enum

enum class UserStatus {
    INCOMPLETE,
    COMPLETE
}
