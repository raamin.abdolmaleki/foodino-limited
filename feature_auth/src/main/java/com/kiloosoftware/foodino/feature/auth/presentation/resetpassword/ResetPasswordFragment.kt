package com.kiloosoftware.foodino.feature.auth.presentation.resetpassword

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.kiloosoftware.foodino.library.base.presentation.extension.observe
import com.kiloosoftware.foodino.library.base.presentation.fragment.BaseContainerFragment
import com.kiloosoftware.foodino.feature.auth.R
import com.kiloosoftware.foodino.feature.auth.databinding.FragmentResetPasswordBinding
import com.kiloosoftware.foodino.library.base.presentation.extension.setOnDebouncedClickListener
import com.kiloosoftware.foodino.presentation.singleton.AppBarState
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarData
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarStyle
import kotlinx.android.synthetic.main.fragment_reset_password.*
import org.kodein.di.generic.instance

class ResetPasswordFragment : BaseContainerFragment<FragmentResetPasswordBinding>() {

    override val layoutResourceId = R.layout.fragment_reset_password

    private val viewModel: ResetPasswordViewModel by instance()

    private val stateObserver = Observer<ResetPasswordViewModel.ViewState> {
        binding.loading = it.isLoading

        if (it.isSuccess) {
            // back to first page of auth process
            findNavController().popBackStack()
            // pop first page of auth and go back to starting process
            findNavController().popBackStack()
            showMessage(resources.getString(R.string.reset_password_succeed))
        } else if (it.isError) {
            showErrors(it.errors) {
                resetPassword()
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btnConfirmation.setOnDebouncedClickListener {
            resetPassword()
        }

        tvChoosePasswordLater.setOnDebouncedClickListener {
            // back to first page of auth process
            findNavController().popBackStack()
            // pop first page of auth and go back to starting process
            findNavController().popBackStack()
        }
        observe(viewModel.stateLiveData, stateObserver)
    }

    private fun resetPassword() {
        viewModel.resetPassword(etPassword.editableText.toString())
    }

    override fun setToolbar() {
        AppBarState.style = AppBarData(AppBarStyle.TITLE_WITH_BACK)
    }
}
