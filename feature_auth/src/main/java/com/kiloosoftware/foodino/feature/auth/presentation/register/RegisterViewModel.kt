package com.kiloosoftware.foodino.feature.auth.presentation.register

import androidx.lifecycle.viewModelScope
import com.adjust.sdk.Adjust
import com.adjust.sdk.AdjustEvent
import com.kiloosoftware.foodino.feature.auth.domain.usecase.RegisterUseCase
import com.kiloosoftware.foodino.feature.auth.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.library.base.data.Error
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseAction
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewModel
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewState
import kotlinx.coroutines.launch

internal class RegisterViewModel(
    private val registerUseCase: RegisterUseCase,
    private val firebaseEvents: FirebaseEvents
) : BaseViewModel<RegisterViewModel.ViewState, RegisterViewModel.Action>(
    ViewState()
) {

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.RegisterSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isSuccess = true,
            errors = null
        )
        is Action.RegisterFailure -> state.copy(
            isLoading = false,
            isError = true,
            isSuccess = false,
            errors = viewAction.errors
        )
        is Action.RegisterLoading -> state.copy(
            isLoading = true,
            isError = false,
            isSuccess = false,
            errors = null
        )
    }

    fun registerUser(fullName: String, password: String) {
        sendAction(Action.RegisterLoading)
        viewModelScope.launch {
            registerUseCase.execute(fullName, password).also {
                if (it.statusCode == StatusCode.OK) {
                    sendAction(Action.RegisterSuccess)
                    firebaseEvents.registerSuccess()
                    adjustRegisterSuccess()
                } else {
                    sendAction(Action.RegisterFailure(it.errors))
                }
            }
        }
        firebaseEvents.registerClicked()
    }

    private fun adjustRegisterSuccess() {
        val adjustEvent = AdjustEvent("supr0b")
        Adjust.trackEvent(adjustEvent)
    }

    internal data class ViewState(
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val isSuccess: Boolean = false,
        val errors: List<Error>? = null
    ) : BaseViewState

    internal sealed class Action : BaseAction {
        object RegisterSuccess: Action()
        class RegisterFailure(val errors: List<Error>?) : Action()
        object RegisterLoading : Action()
    }
}
