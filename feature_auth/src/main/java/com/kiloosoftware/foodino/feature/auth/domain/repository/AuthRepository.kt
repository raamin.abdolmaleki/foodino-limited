package com.kiloosoftware.foodino.feature.auth.domain.repository

import com.kiloosoftware.foodino.feature.auth.data.model.response.AuthenticateUserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.VerificationCodeDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserValidationDataModel
import com.kiloosoftware.foodino.library.base.data.Resource

internal interface AuthRepository {
    suspend fun validateUser(phoneNumber: String): Resource<UserValidationDataModel>

    suspend fun authenticateUser(
        phoneNumber: String,
        password: String,
        ott: String
    ): Resource<AuthenticateUserDataModel>

    suspend fun registerUser(
        fullName: String,
        password: String
    ): Resource<Any>

    suspend fun sendVerificationCode(
        phoneNumber: String
    ): Resource<VerificationCodeDataModel>

    suspend fun resetPassword(
        fullName: String,
        newPassword: String
    ): Resource<Any>

    suspend fun getUserInfo(): Resource<UserDataModel>
}
