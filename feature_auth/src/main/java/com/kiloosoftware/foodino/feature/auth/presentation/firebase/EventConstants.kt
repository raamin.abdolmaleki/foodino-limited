package com.kiloosoftware.foodino.feature.auth.presentation.firebase

object EventConstants {
    const val AUTH_START = "auth_start"
    const val AUTH_CELL_VERIFIED = "auth_cell_verify"
    const val AUTH_SET_PASSWORD = "auth_set_password"
    const val REGISTER_CLICKED = "auth_sign_up"
}
