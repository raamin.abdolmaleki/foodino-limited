package com.kiloosoftware.foodino.feature.auth.data.repository

import com.kiloosoftware.foodino.feature.auth.BuildConfig
import com.kiloosoftware.foodino.feature.auth.data.model.request.SendVerificationCodeRequestModel
import com.kiloosoftware.foodino.feature.auth.data.model.request.SetUserInfoRequestModel
import com.kiloosoftware.foodino.feature.auth.data.model.request.UserValidationRequestModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.AuthenticateUserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.VerificationCodeDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserValidationDataModel
import com.kiloosoftware.foodino.feature.auth.data.retrofit.service.AuthRetrofitService
import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository
import com.kiloosoftware.foodino.library.base.data.Resource
import com.kiloosoftware.foodino.library.base.data.safeApiCall

internal class AuthRepositoryImpl(
    private val authRetrofitService: AuthRetrofitService
) : AuthRepository {

    override suspend fun validateUser(phoneNumber: String): Resource<UserValidationDataModel> {
        return safeApiCall(call = {
            authRetrofitService.validateUserAsync(
                UserValidationRequestModel(
                    phoneNumber
                )
            )
        })
    }

    override suspend fun authenticateUser(
        phoneNumber: String,
        password: String,
        ott: String
    ): Resource<AuthenticateUserDataModel> {
        return safeApiCall(call = {
            authRetrofitService.authenticateUserAsync(
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                phoneNumber,
                password,
                ott
            )
        })
    }

    override suspend fun registerUser(fullName: String, password: String): Resource<Any> {
        return safeApiCall(call = {
            authRetrofitService.setUserInfoAsync(
                SetUserInfoRequestModel(
                    fullName,
                    password
                )
            )
        })
    }

    override suspend fun resetPassword(fullName: String, newPassword: String): Resource<Any> {
        return safeApiCall(call = {
            authRetrofitService.setUserInfoAsync(
                SetUserInfoRequestModel(
                    fullName,
                    newPassword
                )
            )
        })
    }

    override suspend fun sendVerificationCode(phoneNumber: String): Resource<VerificationCodeDataModel> {
        return safeApiCall(call = {
            authRetrofitService.sendVerificationCodeAsync(
                SendVerificationCodeRequestModel(phoneNumber)
            )
        })
    }

    override suspend fun getUserInfo(): Resource<UserDataModel> {
        return safeApiCall(call = { authRetrofitService.getUserInfoAsync() })
    }
}
