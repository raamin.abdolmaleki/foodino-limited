package com.kiloosoftware.foodino.feature.auth.data.model.response

import com.squareup.moshi.Json

internal data class UserDataModel(
    @field:Json(name = "id") val id: String?,
    @field:Json(name = "full_name") val fullName: String?,
    @field:Json(name = "phone") val phone: String?,
    @field:Json(name = "balance") val balance: Long?,
    @field:Json(name = "is_active") val isActive: Boolean
)
