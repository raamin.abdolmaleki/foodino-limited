package com.kiloosoftware.foodino.feature.auth.presentation.userverification

import androidx.lifecycle.viewModelScope
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserDataModel
import com.kiloosoftware.foodino.feature.auth.domain.usecase.SendVerificationCodeUseCase
import com.kiloosoftware.foodino.feature.auth.domain.usecase.UserInfoUseCase
import com.kiloosoftware.foodino.feature.auth.domain.usecase.VerifyUserUseCase
import com.kiloosoftware.foodino.feature.auth.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.library.base.data.Error
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseAction
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewModel
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewState
import com.kiloosoftware.foodino.library.base.utils.sharedpreference.UserPreferences
import kotlinx.coroutines.launch

internal class UserVerificationViewModel(
    private val verifyUserUseCase: VerifyUserUseCase,
    private val userInfoUseCase: UserInfoUseCase,
    private val sendVerificationCodeUseCase: SendVerificationCodeUseCase,
    private val userPreferences: UserPreferences,
    private val args: UserVerificationFragmentArgs,
    private val firebaseEvents: FirebaseEvents
) : BaseViewModel<UserVerificationViewModel.ViewState, UserVerificationViewModel.Action>(
    ViewState()
) {

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.UserVerificationSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isSuccess = true,
            isVerificationResendCodeSuccess = false,
            errors = null,
            data = viewAction.data
        )
        is Action.UserVerificationFailure -> state.copy(
            isLoading = false,
            isError = true,
            isSuccess = false,
            isVerificationResendCodeSuccess = false,
            errors  = viewAction.errors ,
            data = null
        )
        is Action.UserVerificationLoading -> state.copy(
            isLoading = true,
            isError = false,
            isSuccess = false,
            isVerificationResendCodeSuccess = false,
            errors  = null,
            data = null
        )
        Action.UserVerificationResendCodeSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isSuccess = false,
            isVerificationResendCodeSuccess = true,
            errors  = null,
            data = null
        )
    }

    fun verifyUser(verificationCode: String) {
        sendAction(Action.UserVerificationLoading)
        viewModelScope.launch {
            verifyUserUseCase.execute(args.phoneNumber, verificationCode, args.ott).also {
                if (it.statusCode == StatusCode.OK && it.data != null) {
                    userPreferences.setLoginInfo(
                        accessToken = it.data!!.accessToken,
                        expiresIn = it.data!!.expiresIn
                    )
                    firebaseEvents.authCellVerified()
                    getUserInfo()
                } else {
                    sendAction(Action.UserVerificationFailure(it.errors))
                }
            }
        }
    }

    private fun getUserInfo() {
        viewModelScope.launch {
            userInfoUseCase.execute().also {
                if (it.statusCode == StatusCode.OK && it.data != null) {
                    sendAction(Action.UserVerificationSuccess(it.data!!))
                } else {
                    sendAction(Action.UserVerificationFailure(it.errors))
                }
            }
        }
    }

    fun resendVerificationCode() {
        userPreferences.logout()
        viewModelScope.launch {
            sendVerificationCodeUseCase.execute(args.phoneNumber).also {
                if (it.statusCode == StatusCode.OK && it.data != null) {
                    sendAction(Action.UserVerificationResendCodeSuccess)
                } else {
                    sendAction(Action.UserVerificationFailure(it.errors))
                }
            }
        }
    }

    fun getArgs() = args

    internal data class ViewState(
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val isSuccess: Boolean = false,
        val isVerificationResendCodeSuccess: Boolean = false,
        val errors: List<Error>? = null,
        val data: UserDataModel? = null
    ) : BaseViewState

    internal sealed class Action : BaseAction {
        class UserVerificationSuccess(val data: UserDataModel) : Action()
        class UserVerificationFailure(val errors: List<Error>?) : Action()
        object UserVerificationLoading : Action()
        object UserVerificationResendCodeSuccess : Action()
    }
}
