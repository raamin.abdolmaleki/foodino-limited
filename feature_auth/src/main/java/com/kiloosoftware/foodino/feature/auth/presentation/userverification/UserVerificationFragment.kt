package com.kiloosoftware.foodino.feature.auth.presentation.userverification

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.kiloosoftware.foodino.library.base.presentation.extension.observe
import com.kiloosoftware.foodino.library.base.presentation.fragment.BaseContainerFragment
import com.kiloosoftware.foodino.feature.auth.R
import com.kiloosoftware.foodino.feature.auth.databinding.FragmentUserVerificationBinding
import com.kiloosoftware.foodino.library.base.presentation.extension.setOnDebouncedClickListener
import com.kiloosoftware.foodino.library.base.utils.Constants
import com.kiloosoftware.foodino.presentation.singleton.AppBarState
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarStyle
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarData
import org.kodein.di.generic.instance
import kotlinx.android.synthetic.main.fragment_user_verification.*

class UserVerificationFragment : BaseContainerFragment<FragmentUserVerificationBinding>() {

    override val layoutResourceId = R.layout.fragment_user_verification

    private val viewModel: UserVerificationViewModel by instance()

    private val stateObserver = Observer<UserVerificationViewModel.ViewState> {
        binding.loading = it.isLoading

        if (it.isSuccess) {
            if (it.data!!.fullName != null && it.data.fullName!!.isNotEmpty()) {
                findNavController().navigate(
                    UserVerificationFragmentDirections.actionVerificationToResetPassword(it.data.fullName)
                )
            } else {
                findNavController().navigate(UserVerificationFragmentDirections.actionVerificationToRegister())
            }
        } else if (it.isError) {
            showErrors(it.errors) {
                verifyUser()
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.phoneNumber = viewModel.getArgs().phoneNumber

        startTimer()

        btnVerification.setOnDebouncedClickListener {
            verifyUser()
        }

        tvResendCode.setOnDebouncedClickListener {
            if (!binding.isResendTimerRunning) {
                startTimer()
                viewModel.resendVerificationCode()
            }
        }

        observe(viewModel.stateLiveData, stateObserver)
    }

    private fun verifyUser() = viewModel.verifyUser(etVerificationCode.editableText.toString())

    private val timer = object : CountDownTimer(Constants.RESEND_CODE_TIMER, 1000) {

        override fun onTick(millisUntilFinished: Long) {
            tvResendCode.text = String.format(
                resources.getString(R.string.resend_verification_code_will_be_send),
                ((millisUntilFinished + 20) / 1000)
            )
        }

        override fun onFinish() {
            tvResendCode.text = resources.getString(R.string.resend_verification_code)
            binding.isResendTimerRunning = false
        }
    }

    private fun startTimer() {
        binding.isResendTimerRunning = true
        timer.start()
    }

    override fun onDestroyView() {
        timer.cancel()
        super.onDestroyView()
    }

    override fun setToolbar() {
        AppBarState.style = AppBarData(AppBarStyle.TITLE_WITH_BACK)
    }
}
