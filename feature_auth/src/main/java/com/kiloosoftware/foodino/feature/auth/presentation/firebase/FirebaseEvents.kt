package com.kiloosoftware.foodino.feature.auth.presentation.firebase

import com.google.firebase.analytics.FirebaseAnalytics

class FirebaseEvents(private val firebaseAnalytics: FirebaseAnalytics) {
    fun authStarted() {
        firebaseAnalytics.logEvent(EventConstants.AUTH_START, null)
    }

    fun login() {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, null)
    }

    fun authCellVerified() {
        firebaseAnalytics.logEvent(EventConstants.AUTH_CELL_VERIFIED, null)
    }

    fun registerSuccess() {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, null)
    }

    fun registerClicked() {
        firebaseAnalytics.logEvent(EventConstants.REGISTER_CLICKED, null)
    }

    fun authSetPassword() {
        firebaseAnalytics.logEvent(EventConstants.AUTH_SET_PASSWORD, null)
    }
}
