package com.kiloosoftware.foodino.feature.auth.data.retrofit.service

import com.kiloosoftware.foodino.feature.auth.data.model.request.SendVerificationCodeRequestModel
import com.kiloosoftware.foodino.feature.auth.data.model.request.SetUserInfoRequestModel
import com.kiloosoftware.foodino.feature.auth.data.model.request.UserValidationRequestModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.AuthenticateUserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.VerificationCodeDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserValidationDataModel
import com.kiloosoftware.foodino.library.base.data.ApiResponse
import retrofit2.Response
import retrofit2.http.*

internal interface AuthRetrofitService {
    @POST("users/self/verification-flow/")
    suspend fun validateUserAsync(
        @Body userValidationRequestModel: UserValidationRequestModel
    ): Response<ApiResponse<UserValidationDataModel>>

    @FormUrlEncoded
    @POST("oauth/token/")
    suspend fun authenticateUserAsync(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("username") phone: String,
        @Field("password") password: String,
        @Field("ott") ott: String,
        @Field("grant_type") grantType: String = "password"
    ): Response<ApiResponse<AuthenticateUserDataModel>>

    @PATCH("users/self/essentials/")
    suspend fun setUserInfoAsync(
        @Body registerUserRequestModel: SetUserInfoRequestModel
    ): Response<ApiResponse<Any>>

    @POST("users/self/reset-password/")
    suspend fun sendVerificationCodeAsync(
        @Body forgotPasswordDataModel: SendVerificationCodeRequestModel
    ): Response<ApiResponse<VerificationCodeDataModel>>

    @GET("users/self")
    suspend fun getUserInfoAsync(): Response<ApiResponse<UserDataModel>>
}
