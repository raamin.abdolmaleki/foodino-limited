package com.kiloosoftware.foodino.feature.auth.domain.usecase

import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository

internal class ResetPasswordUseCase(
    private val authRepository: AuthRepository
) {
    suspend fun execute(fullName: String, newPassword: String) =
        authRepository.resetPassword(fullName, newPassword)
}
