package com.kiloosoftware.foodino.feature.auth.domain.usecase

import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository

internal class UserInfoUseCase(
    private val authRepository: AuthRepository
) {
    suspend fun execute() = authRepository.getUserInfo()
}
