package com.kiloosoftware.foodino.feature.auth.domain

import com.kiloosoftware.foodino.feature.auth.FEATURE_NAME
import com.kiloosoftware.foodino.feature.auth.domain.usecase.*
import com.kiloosoftware.foodino.feature.auth.domain.usecase.LoginUseCase
import com.kiloosoftware.foodino.feature.auth.domain.usecase.RegisterUseCase
import com.kiloosoftware.foodino.feature.auth.domain.usecase.ResetPasswordUseCase
import com.kiloosoftware.foodino.feature.auth.domain.usecase.ValidateUserUseCase
import com.kiloosoftware.foodino.feature.auth.domain.usecase.VerifyUserUseCase
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

internal val domainModule = Kodein.Module("${FEATURE_NAME}DomainModule") {
    bind() from singleton { ValidateUserUseCase(instance()) }
    bind() from singleton { LoginUseCase(instance()) }
    bind() from singleton { VerifyUserUseCase(instance()) }
    bind() from singleton { RegisterUseCase(instance()) }
    bind() from singleton { SendVerificationCodeUseCase(instance()) }
    bind() from singleton { ResetPasswordUseCase(instance()) }
    bind() from singleton { UserInfoUseCase(instance()) }
}
