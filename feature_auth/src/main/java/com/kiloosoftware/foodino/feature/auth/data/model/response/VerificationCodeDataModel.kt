package com.kiloosoftware.foodino.feature.auth.data.model.response

import com.squareup.moshi.Json

internal data class VerificationCodeDataModel(
    @field:Json(name = "ott") val ott: String
)
