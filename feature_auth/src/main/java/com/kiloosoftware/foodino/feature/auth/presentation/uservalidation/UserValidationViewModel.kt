package com.kiloosoftware.foodino.feature.auth.presentation.uservalidation

import androidx.lifecycle.viewModelScope
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserValidationDataModel
import com.kiloosoftware.foodino.feature.auth.domain.usecase.ValidateUserUseCase
import com.kiloosoftware.foodino.feature.auth.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.library.base.data.Error
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.presentation.extension.isValidPhoneNumber
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseAction
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewModel
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewState
import com.kiloosoftware.foodino.library.base.utils.sharedpreference.UserPreferences
import kotlinx.coroutines.launch

internal class UserValidationViewModel(
    private val validateUserUseCase: ValidateUserUseCase,
    userPreferences: UserPreferences,
    private val firebaseEvents: FirebaseEvents
) : BaseViewModel<UserValidationViewModel.ViewState, UserValidationViewModel.Action>(
    ViewState()
) {
    init {
        userPreferences.logout()
    }
    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.UserValidationSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isValidationError = false,
            isSuccess = true,
            errors = null,
            data = viewAction.data
        )
        is Action.UserValidationFailure -> state.copy(
            isLoading = false,
            isError = true,
            isValidationError = false,
            isSuccess = false,
            errors = viewAction.errors,
            data = UserValidationDataModel(
                verificationFlow = "",
                ott = ""
            )
        )
        is Action.UserValidationLoading -> state.copy(
            isLoading = true,
            isError = false,
            isValidationError = false,
            isSuccess = false,
            errors = null,
            data = UserValidationDataModel(
                verificationFlow= "",
                ott = ""
            )
        )
        is Action.InputValidationError ->  state.copy(
            isLoading = false,
            isError = false,
            isValidationError= true,
            isSuccess = false,
            errors = null,
            data = UserValidationDataModel(
                verificationFlow = "",
                ott = ""
            )
        )
    }

    fun validateUser(phoneNumber: String) {
        if (!phoneNumber.isValidPhoneNumber()) {
            sendAction(Action.InputValidationError)
            return
        }
        firebaseEvents.authStarted()
        sendAction(Action.UserValidationLoading)
        viewModelScope.launch {
            validateUserUseCase.execute(phoneNumber).also {
                if (it.statusCode == StatusCode.OK && it.data != null) {
                    sendAction(
                        Action.UserValidationSuccess(it.data!!)
                    )
                } else {
                    sendAction(
                        Action.UserValidationFailure(it.errors)
                    )
                }
            }
        }
    }

    internal data class ViewState(
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val isValidationError: Boolean = false,
        val isSuccess: Boolean = false,
        val errors: List<Error>? = null,
        val data: UserValidationDataModel = UserValidationDataModel(
            verificationFlow = "",
            ott = ""
        )
    ) : BaseViewState

    internal sealed class Action : BaseAction {
        class UserValidationSuccess(val data: UserValidationDataModel) : Action()
        class UserValidationFailure(val errors: List<Error>?) : Action()
        object InputValidationError : Action()
        object UserValidationLoading : Action()
    }
}
