package com.kiloosoftware.foodino.feature.auth.data

import com.kiloosoftware.foodino.feature.auth.BuildConfig
import com.kiloosoftware.foodino.feature.auth.FEATURE_NAME
import com.kiloosoftware.foodino.feature.auth.data.repository.AuthRepositoryImpl
import com.kiloosoftware.foodino.feature.auth.data.repository.mock.AuthRepositoryMockImpl
import com.kiloosoftware.foodino.feature.auth.data.retrofit.service.AuthRetrofitService
import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

internal val dataModule = Kodein.Module("${FEATURE_NAME}DataModule") {

    // if in mock flavor then mock data will be return
    bind<AuthRepository>() with singleton {
        if (!BuildConfig.MOCK_ENABLED) AuthRepositoryImpl(instance()) else AuthRepositoryMockImpl()
    }

    bind() from singleton { instance<Retrofit>().create(AuthRetrofitService::class.java) }
}
