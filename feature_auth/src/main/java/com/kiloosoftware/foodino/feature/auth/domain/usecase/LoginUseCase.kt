package com.kiloosoftware.foodino.feature.auth.domain.usecase

import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository

internal class LoginUseCase(
    private val authRepository: AuthRepository
) {
    suspend fun execute(phoneNumber: String, password: String, ott: String) =
        authRepository.authenticateUser(phoneNumber, password, ott)
}
