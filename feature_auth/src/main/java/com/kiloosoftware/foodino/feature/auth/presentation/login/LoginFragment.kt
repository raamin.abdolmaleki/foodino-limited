package com.kiloosoftware.foodino.feature.auth.presentation.login

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.kiloosoftware.foodino.feature.auth.R
import com.kiloosoftware.foodino.feature.auth.databinding.FragmentLoginBinding
import com.kiloosoftware.foodino.library.base.presentation.extension.observe
import com.kiloosoftware.foodino.library.base.presentation.extension.setOnDebouncedClickListener
import com.kiloosoftware.foodino.library.base.presentation.fragment.BaseContainerFragment
import com.kiloosoftware.foodino.presentation.singleton.AppBarState
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarData
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarStyle
import kotlinx.android.synthetic.main.fragment_login.*
import org.kodein.di.generic.instance

class LoginFragment : BaseContainerFragment<FragmentLoginBinding>() {

    override val layoutResourceId = R.layout.fragment_login

    private val viewModel: LoginViewModel by instance()

    private val stateObserver = Observer<LoginViewModel.ViewState> {
        binding.loading = it.isLoading

        if (it.isError) {
            showErrors(it.errors) {
                viewModel.loginUser(etPassword.editableText.toString())
            }
        }

        if (it.isLoginSuccess) {
            // back to first page of auth process
            findNavController().popBackStack()
            // pop first page of auth and go back to starting process
            findNavController().popBackStack()
        } else if (it.isVerificationCodeSuccess) {
            val navDirections = LoginFragmentDirections.actionLoginToVerification(
                ott = it.sendVerificationCodeData!!.ott,
                phoneNumber = viewModel.getArgs().phoneNumber,
                verificationFlow = viewModel.getArgs().verificationFlow
            )
            findNavController().navigate(navDirections)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btnConfirmation.setOnDebouncedClickListener {
            viewModel.loginUser(etPassword.editableText.toString())
        }

        tvForgotPassword.setOnDebouncedClickListener {
            viewModel.forgotPassword()
        }

        observe(viewModel.stateLiveData, stateObserver)
    }

    override fun setToolbar() {
        AppBarState.style = AppBarData(AppBarStyle.TITLE_WITH_BACK)
    }
}
