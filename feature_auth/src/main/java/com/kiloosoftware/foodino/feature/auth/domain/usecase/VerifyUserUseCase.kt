package com.kiloosoftware.foodino.feature.auth.domain.usecase

import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository

internal class VerifyUserUseCase(
    private val authRepository: AuthRepository
) {
    suspend fun execute(phoneNumber: String, verificationCode: String, ott: String) =
        authRepository.authenticateUser(phoneNumber, verificationCode, ott)
}
