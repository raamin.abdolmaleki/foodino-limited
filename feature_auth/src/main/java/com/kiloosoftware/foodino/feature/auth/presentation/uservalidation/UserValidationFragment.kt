package com.kiloosoftware.foodino.feature.auth.presentation.uservalidation

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.kiloosoftware.foodino.library.base.presentation.extension.observe
import com.kiloosoftware.foodino.library.base.presentation.fragment.BaseContainerFragment
import com.kiloosoftware.foodino.feature.auth.R
import com.kiloosoftware.foodino.feature.auth.databinding.FragmentUserValidationBinding
import com.kiloosoftware.foodino.library.base.data.enum.VerificationFlow
import com.kiloosoftware.foodino.library.base.presentation.extension.setOnDebouncedClickListener
import com.kiloosoftware.foodino.presentation.singleton.AppBarState
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarData
import com.kiloosoftware.foodino.presentation.singleton.AppBarState.AppBarStyle
import org.kodein.di.generic.instance
import kotlinx.android.synthetic.main.fragment_user_validation.*

class UserValidationFragment : BaseContainerFragment<FragmentUserValidationBinding>() {

    override val layoutResourceId = R.layout.fragment_user_validation

    private val viewModel: UserValidationViewModel by instance()

    private val stateObserver = Observer<UserValidationViewModel.ViewState> {

        binding.loading = it.isLoading

        if (it.isValidationError) {
            tilPhoneNumber.error = resources.getString(R.string.please_enter_correct_phone_number)
        } else if (it.isError) {
            showErrors(it.errors) {
                validateUser()
            }
        } else if (it.isSuccess) {
            if (it.data.verificationFlow == VerificationFlow.PASSWORD_VERIFICATION.flow) {
                val navDirection =
                    UserValidationFragmentDirections.actionValidationToLogin(
                        phoneNumber = etPhoneNumber.editableText.toString(),
                        verificationFlow = it.data.verificationFlow,
                        ott = it.data.ott
                    )
                findNavController().navigate(navDirection)
            } else {
                val navDirection =
                    UserValidationFragmentDirections.actionValidationToVerification(
                        phoneNumber = etPhoneNumber.editableText.toString(),
                        verificationFlow = it.data.verificationFlow,
                        ott = it.data.ott
                    )
                findNavController().navigate(navDirection)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btnLogin.setOnDebouncedClickListener {
            validateUser()
        }
        observe(viewModel.stateLiveData, stateObserver)
    }

    private fun validateUser() {
        viewModel.validateUser(etPhoneNumber.editableText.toString())
    }

    override fun setToolbar() {
        AppBarState.style = AppBarData(AppBarStyle.TITLE_WITH_CLOSE)
    }
}
