package com.kiloosoftware.foodino.feature.auth.presentation

import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.kiloosoftware.foodino.library.base.R

@BindingAdapter("app:active")
fun AppCompatTextView.active(active: Boolean) {
    when(active) {
        true -> this.setTextColor(ContextCompat.getColor(this.context, R.color.text_color_gray_400))
        false -> this.setTextColor(ContextCompat.getColor(this.context, R.color.colorPrimary))
    }
}
