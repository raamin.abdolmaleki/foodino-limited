package com.kiloosoftware.foodino.feature.auth.data.model.response

import com.squareup.moshi.Json

internal data class AuthenticateUserDataModel(
    @field:Json(name = "user_status") val userStatus : String,
    @field:Json(name = "access_token") val accessToken: String,
    @field:Json(name = "expires_in") val expiresIn: Int
)
