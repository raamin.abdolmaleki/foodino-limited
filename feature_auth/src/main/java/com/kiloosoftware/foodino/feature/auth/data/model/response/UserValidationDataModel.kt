package com.kiloosoftware.foodino.feature.auth.data.model.response

import com.squareup.moshi.Json

internal data class UserValidationDataModel(
    @field:Json(name = "verification_flow") val verificationFlow: String = "",
    @field:Json(name = "ott") val ott : String = ""
)
