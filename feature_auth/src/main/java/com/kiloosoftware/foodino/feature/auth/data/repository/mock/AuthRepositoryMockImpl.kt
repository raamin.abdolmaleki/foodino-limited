package com.kiloosoftware.foodino.feature.auth.data.repository.mock

import com.kiloosoftware.foodino.feature.auth.data.enum.UserStatus
import com.kiloosoftware.foodino.feature.auth.data.model.response.AuthenticateUserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.VerificationCodeDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserDataModel
import com.kiloosoftware.foodino.feature.auth.data.model.response.UserValidationDataModel
import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository
import com.kiloosoftware.foodino.library.base.data.Resource
import com.kiloosoftware.foodino.library.base.data.enum.VerificationFlow
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.utils.Constants.MOCK_DELAY_TIME
import kotlinx.coroutines.delay

internal class AuthRepositoryMockImpl : AuthRepository {

    override suspend fun validateUser(phoneNumber: String): Resource<UserValidationDataModel> {
        delay(MOCK_DELAY_TIME)

        val response =
            UserValidationDataModel(
                ott = "ott",
                verificationFlow = VerificationFlow.PASSWORD_VERIFICATION.flow
            )

        return Resource(
            data = response,
            statusCode = StatusCode.OK
        )
    }

    override suspend fun authenticateUser(
        phoneNumber: String,
        password: String,
        ott: String
    ): Resource<AuthenticateUserDataModel> {
        delay(MOCK_DELAY_TIME)

        val response =
            AuthenticateUserDataModel(
                userStatus = UserStatus.COMPLETE.name,
                accessToken = "ACCESS_TOKEN",
                expiresIn = 3600
            )

        return Resource(
            data = response,
            statusCode = StatusCode.OK
        )
    }

    override suspend fun registerUser(fullName: String, password: String): Resource<Any> {
        delay(MOCK_DELAY_TIME)

        val response = Any()

        return Resource(
            data = response,
            statusCode = StatusCode.OK
        )
    }

    override suspend fun sendVerificationCode(phoneNumber: String): Resource<VerificationCodeDataModel> {
        val response = VerificationCodeDataModel(
            ott = "OTT"
        )
        return Resource(
            data = response,
            statusCode = StatusCode.OK
        )
    }

    override suspend fun resetPassword(fullName: String, newPassword: String): Resource<Any> {
        delay(MOCK_DELAY_TIME)

        val response = Any()

        return Resource(
            data = response,
            statusCode = StatusCode.OK
        )
    }

    override suspend fun getUserInfo(): Resource<UserDataModel> {
        delay(MOCK_DELAY_TIME)

        val response = UserDataModel("ID", "رامین عبدالملکی", "029454644", 2342345456, true)

        return Resource(
            data = response,
            statusCode = StatusCode.OK
        )
    }
}
