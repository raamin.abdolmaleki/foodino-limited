package com.kiloosoftware.foodino.feature.auth.domain.usecase

import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository

internal class SendVerificationCodeUseCase(
    private val authRepository: AuthRepository
) {
    suspend fun execute(phoneNumber: String) =
        authRepository.sendVerificationCode(phoneNumber)
}
