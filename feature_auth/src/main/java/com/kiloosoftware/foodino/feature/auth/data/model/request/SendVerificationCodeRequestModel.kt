package com.kiloosoftware.foodino.feature.auth.data.model.request

import com.squareup.moshi.Json

internal class SendVerificationCodeRequestModel(
    @field:Json(name = "username") val phoneNumber: String
)
