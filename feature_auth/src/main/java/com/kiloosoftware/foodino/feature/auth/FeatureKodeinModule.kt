package com.kiloosoftware.foodino.feature.auth

import com.kiloosoftware.foodino.feature.KodeinModuleProvider
import com.kiloosoftware.foodino.feature.auth.data.dataModule
import com.kiloosoftware.foodino.feature.auth.domain.domainModule
import com.kiloosoftware.foodino.feature.auth.presentation.presentationModule

import org.kodein.di.Kodein

internal const val FEATURE_NAME = "Auth"

object FeatureKodeinModule : KodeinModuleProvider {

    override val kodeinModule = Kodein.Module("${FEATURE_NAME}Module") {
        import(presentationModule)
        import(domainModule)
        import(dataModule)
    }
}
