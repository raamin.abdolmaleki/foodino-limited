package com.kiloosoftware.foodino.feature.auth.domain.usecase

import com.kiloosoftware.foodino.feature.auth.domain.repository.AuthRepository

internal class ValidateUserUseCase(
    private val authRepository: AuthRepository
) {
    suspend fun execute(phoneNumber: String) = authRepository.validateUser(phoneNumber)
}
