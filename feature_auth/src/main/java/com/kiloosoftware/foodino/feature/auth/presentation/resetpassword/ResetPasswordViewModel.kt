package com.kiloosoftware.foodino.feature.auth.presentation.resetpassword

import androidx.lifecycle.viewModelScope
import com.kiloosoftware.foodino.feature.auth.domain.usecase.ResetPasswordUseCase
import com.kiloosoftware.foodino.feature.auth.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.library.base.data.Error
import com.kiloosoftware.foodino.library.base.enum.StatusCode
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseAction
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewModel
import com.kiloosoftware.foodino.library.base.presentation.viewmodel.BaseViewState
import kotlinx.coroutines.launch

internal class ResetPasswordViewModel(
    private val resetPasswordUseCase: ResetPasswordUseCase,
    private val args: ResetPasswordFragmentArgs,
    private val firebaseEvents: FirebaseEvents
) : BaseViewModel<ResetPasswordViewModel.ViewState, ResetPasswordViewModel.Action>(
    ViewState()
) {

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.ResetPasswordSuccess -> state.copy(
            isLoading = false,
            isError = false,
            isSuccess = true,
            errors = null
        )
        is Action.ResetPasswordFailure -> state.copy(
            isLoading = false,
            isError = true,
            isSuccess = false,
            errors = viewAction.errors
        )
        is Action.ResetPasswordLoading -> state.copy(
            isLoading = true,
            isError = false,
            isSuccess = false,
            errors = null
        )
    }

    fun resetPassword(newPassword: String) {
        sendAction(Action.ResetPasswordLoading)
        viewModelScope.launch {
            resetPasswordUseCase.execute(args.fullName, newPassword).also {
                if (it.statusCode == StatusCode.OK) {
                    sendAction(Action.ResetPasswordSuccess)
                    firebaseEvents.authSetPassword()
                } else {
                    sendAction(Action.ResetPasswordFailure(it.errors))
                }
            }
        }
    }

    internal data class ViewState(
        val isLoading: Boolean = false,
        val isError: Boolean = false,
        val isSuccess: Boolean = false,
        val errors: List<Error>? = null
    ) : BaseViewState

    internal sealed class Action : BaseAction {
        object ResetPasswordSuccess : Action()
        class ResetPasswordFailure(val errors: List<Error>?) : Action()
        object ResetPasswordLoading : Action()
    }
}
