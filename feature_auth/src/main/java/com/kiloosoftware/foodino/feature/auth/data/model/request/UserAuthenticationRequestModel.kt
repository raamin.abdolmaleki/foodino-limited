package com.kiloosoftware.foodino.feature.auth.data.model.request

import com.squareup.moshi.Json

internal class UserAuthenticationRequestModel(
    @field:Json(name = "client_id") val clientId: String,
    @field:Json(name = "client_secret") val clientSecret: String,
    @field:Json(name = "username") val phone: String,
    @field:Json(name = "password") val password: String,
    @field:Json(name = "grant_type") val grantType: String = "password"
)
