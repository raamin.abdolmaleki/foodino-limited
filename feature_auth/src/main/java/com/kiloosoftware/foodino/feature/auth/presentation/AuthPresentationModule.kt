package com.kiloosoftware.foodino.feature.auth.presentation

import android.content.Context
import androidx.fragment.app.Fragment
import com.kiloosoftware.foodino.feature.auth.FEATURE_NAME
import com.kiloosoftware.foodino.feature.auth.presentation.firebase.FirebaseEvents
import com.kiloosoftware.foodino.feature.auth.presentation.login.LoginViewModel
import com.kiloosoftware.foodino.feature.auth.presentation.register.RegisterViewModel
import com.kiloosoftware.foodino.feature.auth.presentation.resetpassword.ResetPasswordViewModel
import com.kiloosoftware.foodino.feature.auth.presentation.uservalidation.UserValidationViewModel
import com.kiloosoftware.foodino.feature.auth.presentation.userverification.UserVerificationViewModel
import com.kiloosoftware.foodino.library.base.di.KotlinViewModelProvider
import com.kiloosoftware.foodino.library.base.utils.sharedpreference.UserPreferences
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.*

internal val presentationModule = Kodein.Module("${FEATURE_NAME}PresentationModule") {

    bind<UserPreferences>() with factory { context: Context ->
        UserPreferences(
            context
        )
    }

    bind<UserPreferences>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        UserPreferences(
            context.context!!
        )
    }

    bind<FirebaseEvents>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        FirebaseEvents(instance())
    }

    bind<UserValidationViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            UserValidationViewModel(instance(), instance(), instance())
        }
    }

    bind<UserVerificationViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            UserVerificationViewModel(instance(), instance(), instance(), instance(), instance(), instance())
        }
    }

    bind<RegisterViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            RegisterViewModel(instance(), instance())
        }
    }

    bind<LoginViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            LoginViewModel(instance(), instance(), instance(), instance(),instance())
        }
    }

    bind<ResetPasswordViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            ResetPasswordViewModel(instance(), instance(), instance())
        }
    }

}
