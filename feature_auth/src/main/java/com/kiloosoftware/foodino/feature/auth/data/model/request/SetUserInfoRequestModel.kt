package com.kiloosoftware.foodino.feature.auth.data.model.request

import com.squareup.moshi.Json

internal class SetUserInfoRequestModel(
    @field:Json(name = "full_name") val fullName: String,
    @field:Json(name = "password") val password: String
)
