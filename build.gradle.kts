plugins {
    id(GradlePluginId.GRADLE_VERSION_PLUGIN)
    id(GradlePluginId.KOTLIN_JVM) apply false
    id(GradlePluginId.KOTLIN_ANDROID) apply false
    id(GradlePluginId.KOTLIN_ANDROID_EXTENSIONS) apply false
    id(GradlePluginId.ANDROID_APPLICATION) apply false
    id(GradlePluginId.ANDROID_DYNAMIC_FEATURE) apply false
    id(GradlePluginId.ANDROID_LIBRARY) apply false
    id(GradlePluginId.SAFE_ARGS) apply false
}


buildscript {

    repositories {
        google()
        maven(url = "https://maven.fabric.io/public")
    }

    dependencies {
        // Add the following line:
        classpath("com.github.dcendents:android-maven-plugin:1.2")
        classpath("com.jfrog.bintray.gradle:gradle-bintray-plugin:1.7.3")

        classpath(LibraryDependency.GOOGLE_SERVICES)
        classpath(LibraryDependency.FABRIC)
    }
}

// all projects = root project + sub projects
allprojects {
    repositories {
        google()
        jcenter()
    }
}
